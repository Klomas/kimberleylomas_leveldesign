﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManagerScript : MonoBehaviour {
    public Text narrativeText;

	// Use this for initialization
	void Start ()
    {
        Invoke("IntroText1", 3);
	}
	
	void IntroText1()
    {
        narrativeText.text = "intro text placeholder";
        Invoke("IntroText2", 3);
    }

    void IntroText2()
    {
        narrativeText.text = "intro text placeholder 2";
        Invoke("IntroText3", 3);
    }

    void IntroText3()
    {
        narrativeText.text = "intro text placeholder 3";
        Invoke("BlankText", 3);
    }

    void BlankText()
    {
        narrativeText.text = null;
    }
}
