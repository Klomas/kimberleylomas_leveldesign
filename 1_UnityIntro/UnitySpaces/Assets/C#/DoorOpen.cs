﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorOpen : MonoBehaviour {
    public Animation anim;    
    public bool bookMoved = false;

    // Use this for initialization
    void Start ()
    {
        anim = GetComponent<Animation>();
        bookMoved = false;
    }
	
	// Update is called once per frame
	void Update ()
    {
        
    }

}
