﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorPanelScript : MonoBehaviour {

    public AudioSource audioSource;
    public Animation animA;
    public Animation animB;
    public bool switchAdown = false;
    public bool switchBdown = false;
    public TriggerListener triggerA;
    public TriggerListener triggerB;
    public GameObject switchA;
    public GameObject switchB;
    

	// Use this for initialization
	void Start ()
    {
        audioSource = this.GetComponent<AudioSource>();
        animA = GetComponent<Animation>();
        animB = GetComponent<Animation>();
    }
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    void OnTriggerEnter(Collider col)
    {
        Debug.Log("Player Stood on Panel");
        if (triggerA.playerEntered == true)
        {
            animA.Stop();
            animA.Play();
        }


        if (triggerB.playerEntered == true)
        {
            animB.Stop();
            animB.Play();
        }
    }

   
}
