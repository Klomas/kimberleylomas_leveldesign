﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorFloorScript : MonoBehaviour
{

    public float elevatorMoveAmount = 1f;
    public Transform Elevator;
    public float speed = 0.5f;
    public float snapDistance = 0.01f;



  

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {            
            StopCoroutine("ElevatorMove");            
            StartCoroutine("ElevatorMove", 16.51f);
            Debug.Log("Stuff");
        }
    }

    /*void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")            
        {           
            StopCoroutine("ElevatorMove");
            StartCoroutine("ElevatorMove", 0f);           
        }
    }*/


    IEnumerator ElevatorMove(float target)
    {
        //invokes the coroutine
        print(Time.time);
        yield return new WaitForSeconds(3);
        print(Time.time);

        //moves platform
        float yPos = Elevator.localPosition.y;
        float startY = yPos;
        while (yPos > ((startY - target) + snapDistance) || yPos < (target - snapDistance)) 
        {
            yPos = Mathf.Lerp(Elevator.localPosition.y, -target, Time.deltaTime * speed);
            Elevator.localPosition = new Vector3(0, yPos, 0);            
            Debug.Log("Moving");
            yield return null;
        }
        Elevator.localPosition = new Vector3(0, -target, 0);        
        Debug.Log("Stopped");
        yield return null;
    }
}
