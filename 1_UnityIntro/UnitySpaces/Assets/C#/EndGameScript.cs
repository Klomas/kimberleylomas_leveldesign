﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndGameScript : MonoBehaviour
{
    public GameObject Panel;
    public Text screenText;

    void OnTriggerEnter(Collider col)
    {
        Panel.SetActive(true);
        screenText.text = "Game Complete - Restarting Level";
        Debug.Log("End Game");        
        Invoke("Restart", 3F);
        
    }

    void Restart()
    {
        SceneManager.LoadScene(0);
    }
}
