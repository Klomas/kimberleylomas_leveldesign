﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomDoorOpen : MonoBehaviour
{

    public Animation doorAnim;
    public GameObject door;

    private void Start()
    {
        doorAnim = door.GetComponent<Animation>();
        GetComponent<GameObject>();
    }


    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Door Trigger Entered");
        Invoke("DoorOpen", 8f);
    }

    void DoorOpen()
    {        
        {            
            doorAnim.Stop();
            doorAnim.Play();
        }

    }


}
