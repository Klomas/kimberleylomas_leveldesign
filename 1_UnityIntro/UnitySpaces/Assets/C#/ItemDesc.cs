﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemDesc : MonoBehaviour {

    public GameObject Panel;
    public TriggerListener trigger;
    public Image cursorImage;
    public Text screenText;
    private Color startcolor;
    MeshRenderer meshRend;
    

   void Start()
    {
        meshRend = GetComponent<MeshRenderer>();
    }

     

    //highlights item to show interactivity. Returns to original colour when mouse exits
    void OnMouseEnter()
    {
        startcolor = meshRend.material.color;
        meshRend.material.color = new Color (0.8f, 0.8f, 0.8f, 0.25f);
    }
    void OnMouseExit()
    {
        meshRend.material.color = startcolor;
        Panel.SetActive(false);
        screenText.text = "";
    }

    void OnMouseOver()
    {
        Debug.Log("Mouse is over me!!!!!!!!");
        //turn on cursor when enters switch trigger with cursor over switch  
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled != true)
            {
                cursorImage.enabled = true;
                screenText.text = "Read";                

            }           
        }
        //turn off cursor when player leaves trigger with cursor over switch  
        else
        {
            cursorImage.enabled = false;
            screenText.text = "";
           
        }
    }

    void OnMouseDown()
    {
        Debug.Log("Mouse is over me!!!!!!!!");
        //turn on cursor when enters switch trigger with cursor over switch  
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled == true)
            {
                
                Invoke("OpenPanel", 0.2f);
                
            }
        }
        //turn off cursor when player leaves trigger with cursor over switch  
        else
        {
            
            cursorImage.enabled = false;
            screenText.text = "";
        }
    }


    public void OpenPanel()
    {
        if(Panel != null) 
        {
            Panel.SetActive(true);
            screenText.text = "Newspaper Clipping Placeholder Description";
           
        }
        /*else
        {
            Panel.SetActive(false);
            screenText.text = "";
        }*/

    }
}
