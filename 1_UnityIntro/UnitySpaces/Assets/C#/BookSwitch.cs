﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BookSwitch : MonoBehaviour
{

    public AudioSource audioSource;
    public Animation anim;
    public TriggerListener trigger;
    public Image cursorImage;
    public bool switchMoved = false;    
    public Animation doorAnim;
    public GameObject door;
    public bool doorMoved = false;
    
    
    


    // Use this for initialization
    void Start()
    {
        audioSource = this.GetComponent<AudioSource>();
        anim = GetComponent<Animation>();
        cursorImage.enabled = false;        
        GetComponent<GameObject>();
        anim = GetComponent<Animation>();
        switchMoved = false;        
        doorAnim = door.GetComponent<Animation>();
    }

    void OnMouseOver()
    {
        Debug.Log("Mouse is over me!!!!!!!!");
        //turn on cursor when enters switch trigger with cursor over switch  
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled != true)
            {
                cursorImage.enabled = true;
            }
        }
        //turn off cursor when player leaves trigger with cursor over switch  
        else
        {
            cursorImage.enabled = false;
        }
    }

    void OnMouseExit()
    {
        //turn off cursor when exits switch      
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
        }
    }

    void OnMouseDown()
    {
        //toggles switch when player interacts with it
        if (trigger.playerEntered == true)
        {

            anim.Stop();
            anim.Play();
            switchMoved = true;
            Invoke("DoorOpen", 2f);
        }

        
            
    }
          
        
       
    

    

    void DoorOpen()
    {
        if (switchMoved == true)
        {
            doorMoved = true;
            doorAnim.Stop();
            doorAnim.Play();
        }

    }  


    
    
    

    
   

}
