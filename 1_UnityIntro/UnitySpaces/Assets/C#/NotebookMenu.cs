﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotebookMenu : MonoBehaviour {

    public static bool NotebookIsPressed = false;
    public GameObject NotebookMenuUI;

    //update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Keypad4))
        {
            if (NotebookIsPressed)
            {
                Resume();
            }
            else

            {
                Pause();
            }
        }
    }

    
    void Resume()
        {
            NotebookMenuUI.SetActive(false);
            Time.timeScale = 1f;
            NotebookIsPressed = false;

        }
        void Pause()
        {
            NotebookMenuUI.SetActive(true);
            Time.timeScale = 1f;
            NotebookIsPressed = true;
        }
    }


