﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BookShelvesSwitch : MonoBehaviour
{

    public AudioSource audioSource;
    public Animation anim;
    public TriggerListener trigger;    
    public Image cursorImage;
    public Text screenText;

    // Use this for initialization
    void Start()
    {
        audioSource = this.GetComponent<AudioSource>();
        anim = GetComponent<Animation>();        
        cursorImage.enabled = false;

    }

    void OnMouseOver()
    {
        Debug.Log("Mouse is over me!!!!!!!!");
        //turn on cursor when enters switch trigger with cursor over switch  
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled != true)
            {
                cursorImage.enabled = true;
                screenText.text = "It doesn't look like one of these books.";
                CancelInvoke();
                Invoke("BlankText", 2f);
            }
        }

               //turn off cursor when player leaves trigger with cursor over switch  
        else
        {
            cursorImage.enabled = false;
            screenText.text = "";
        }
    }

    void BlankText()
    {
        screenText.text = "";
    }

    void OnMouseExit()
    {
        //turn off cursor when exits switch      
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
            screenText.text = "";
        }
    }

    /*
    void OnMouseDown()
    {
        //toggles switch when player interacts with it
        if (trigger.playerEntered == true)
        {
          screenText.text = "It doesn't look like one of these books.";
        }
    }*/
}
