﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Slidingdoor : MonoBehaviour
{
    public Transform door1Tran;
    public Transform door2Tran;
    public float moveAmount = 1f;
    public float snap = 0.02f;
    public float speed = 5f;


    // Use this for initialization
    void Start () {
		
	}
	
	    //open door
    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Player")
        {
            Debug.Log("Player in trigger");
            //door1Tran.localPosition = new Vector3(moveAmount, 0, 0);
            //door2Tran.localPosition = new Vector3(-moveAmount, 0, 0);
            StopCoroutine("DoorMove");
            StartCoroutine("DoorMove", moveAmount);
        }
    }

    //close door
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            Debug.Log("Player left trigger");
            //door1Tran.localPosition = Vector3.zero;
            //door2Tran.localPosition = Vector3.zero;
            StopCoroutine("DoorMove");
            StartCoroutine("DoorMove", 0f);
        }
    }


    /* Creates a variable where the door already is. If 'x' position of the player compared to the door is less than the given amount (0.98f),
     * lerp between the two points (A to B) over deltatime. It will gradually move the door from A to B smoothly.      
    */

    IEnumerator DoorMove(float target)
    {
        float xPos = door1Tran.localPosition.x;
        Debug.Log("Started");
        while(xPos < (target - snap) || xPos > (target + snap))
        {
            xPos = Mathf.Lerp(door1Tran.localPosition.x, target, speed * Time.deltaTime);
            door1Tran.localPosition = new Vector3(xPos, 0, 0);
            door2Tran.localPosition = new Vector3(-xPos, 0, 0);
            Debug.Log("Moving");
            yield return null;
        }
        door1Tran.localPosition = new Vector3(target, 0, 0);
        door2Tran.localPosition = new Vector3(-target, 0, 0);
        Debug.Log("Finished");
        yield return null;
    }
}
